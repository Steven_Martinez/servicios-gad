POSTGRESQL = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'OPTIONS': {
            'options': '-c search_path=django,public'
        },
        'NAME': 'servicios_gad',
        'USER': 'develop',
        'PASSWORD': 'develop',
        'HOST': 'localhost',
        'PORT': '5432',

    },
}